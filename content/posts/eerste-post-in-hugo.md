+++
title = "Eerste Post in Hugo"
date = 2020-11-25T16:26:13+01:00
description = "Mijn blog krijgt nog maar eens een makeover."
categories = ["static"]
tags = ["indieweb", "static", "no-database"]
+++

Mijn server heeft het begeven, mijn database server om precies te zijn. Aangezien ik geen sysadmin ben, noch de ambitie om er één te worden, vereenvoudig ik mijn tech stack: terug naar static.